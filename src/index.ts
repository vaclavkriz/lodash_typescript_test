import _ from 'lodash';

interface Person {
    name: string;
    item: Item

}

interface Item {
    name: string;
    price: number;
}


const people: Person[] = [
    {
        name: 'Václav',
        item: {name: 'Pes', price: 13}
    }, 
    {
        name: 'Petr',
        item: {name: 'Kočka', price: 1}
    }
]


const filteredPeople: Person[] = _.filter(people, person => !_.isNull(person.name));

console.log(filteredPeople);

const person: Person | undefined = _.find(people, person => person.name === 'Václav');

console.log(person);